<?php

namespace App\Controller;

use App\Entity\Panier;
use App\Entity\Produit;
use App\Entity\Option;
use App\Form\PanierType;
use App\Form\ProduitType;
use App\Repository\OptionRepository;
use App\Repository\PanierRepository;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PanierController extends AbstractController
{
    /**
     * @Route("/panier", name="panier")
     */
    public function index(PanierRepository $PanierRepository): Response
    {
        $total=0;

        $paniers=$PanierRepository->findAll();

        dump($paniers);

        /*for($i=0;$i<$paniers;$i++){

            $products=$paniers[$i].getProducts();

            for($i=0;$i<$paniers;$i++){

                $total=$total+$product.getPrice();

            }

            $options=$panier.getOptions();

            for($i=0;$i<$options;$i++){

                $total=$total+$options[$i].getPrice();

            }
        }*/



        return $this->render('panier/index.html.twig', [
            'controller_name' => 'PanierController',
            'paniers' => $paniers,
            'total' => $total,
        ]);
    }
}
