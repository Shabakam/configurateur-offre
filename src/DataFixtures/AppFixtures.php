<?php

namespace App\DataFixtures;

use App\Entity\Option;
use App\Entity\Panier;
use App\Entity\Produit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // OPTIONS

        $option1 = new Option();
        $option1->setName("Roulettes de chaise");
        $option1->setPrice(5);
        $manager->persist($option1);

        $option2 = new Option();
        $option2->setName("Dossier amovible");
        $option2->setPrice(15);
        $manager->persist($option2);

        $option3 = new Option();
        $option3->setName("Roulettes de table");
        $option3->setPrice(5);
        $manager->persist($option3);

        $option4 = new Option();
        $option4->setName("Table extensible");
        $option4->setPrice(35);
        $manager->persist($option4);

        // PRODUITS

        $produit1 = new Produit();
        $produit1->setName("Chaise premium");
        $produit1->setPrice(20);
        $produit1->addOption($option1);
        $produit1->addOption($option2);
        $manager->persist($produit1);

        $produit2 = new Produit();
        $produit2->setName("Table premium");
        $produit2->setPrice(30);
        $produit2->addOption($option3);
        $produit2->addOption($option4);
        $manager->persist($produit2);

        // PANIER

        $panier1 = new Panier();
        $panier1->addProduct($produit2);
        $panier1->addOption($option3);
        $manager->persist($panier1);

        $panier2 = new Panier();
        $panier2->addProduct($produit1);
        $panier2->addOption($option1);
        $panier2->addOption($option2);
        $manager->persist($panier2);


        $manager->flush();
    }
}
