<?php

namespace App\Controller;

use App\Entity\Panier;
use App\Entity\Produit;
use App\Form\PanierType;
use App\Form\ProduitType;
use App\Repository\OptionRepository;
use App\Repository\PanierRepository;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/produit")
 */
class ProduitController extends AbstractController
{

    public function __construct(EntityManagerInterface $manager)
    {
        $this->entityManager = $manager;
    }

    /**
     * @Route("/", name="produit_index", methods={"GET"})
     */
    public function index(ProduitRepository $produitRepository): Response
    {
        return $this->render('produit/index.html.twig', [
            'produits' => $produitRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="produit_show", methods={"GET"})
     */
    public function show(Produit $produit, Request $request, PanierRepository $panierRepo): Response
    {
        $options = $produit->getOptions();
        
        $panier = $panierRepo->find(1);
        $form = $this->createForm(PanierType::class, $panier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            

            $this->entityManager->persist($panier);
            $this->entityManager->flush();
        }

        return $this->render('produit/show.html.twig', [
            'produit' => $produit,
            'options' => $options,
        ]);
    }

    // /**
    //  * @Route("/{id}/ajout_panier", name="ajout_panier", methods={"POST"})
    //  */
    // public function ajoutPanier(Produit $produit)
    // {
    //     $options = $produit->getOptions();
    //     $optionsChoisies = "";

    //     // return $this->render('produit/show.html.twig', [
    //     //     'produit' => $produit,
    //     //     'options' => $options,
    //     // ]);
    // }
}
